from django.urls import path
from iplcloneapp.views import seasonview,auth
from . import views
from ipl import settings
from django.contrib.staticfiles.urls import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns


urlpatterns=[
    path('getmatches/<int:season>',seasonview.SeasonView.as_view(),name='get_matches'),
    path('getmatches/<int:season>/<int:matchid>',seasonview.DetailView.as_view(),name='get_matchdetails'),
    path('getmatches/<int:season>/<int:matchid>/<int:inning>',seasonview.DetailView.as_view(),name='get_matchdetails_1'),
    path('getpoints/<int:season>/',seasonview.PointTableView.as_view(),name='points_table'),
    path('login/',auth.LoginView.as_view(),name="login_html"),
    path('signup/',auth.SignUpView.as_view(),name="signup_html"),
    path('logout/', auth.Logout, name='logout_html'),
]
urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
