# Generated by Django 2.2.1 on 2019-06-17 05:22

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Match',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('season', models.IntegerField()),
                ('city', models.CharField(max_length=64)),
                ('date', models.DateField()),
                ('team1', models.CharField(max_length=64)),
                ('team2', models.CharField(max_length=64)),
                ('toss_win', models.CharField(max_length=64)),
                ('toss_decision', models.CharField(max_length=10)),
                ('result', models.CharField(max_length=10)),
                ('dl_applied', models.BooleanField()),
                ('winner', models.CharField(max_length=20)),
                ('win_by_runs', models.IntegerField()),
                ('win_by_wickets', models.IntegerField()),
                ('player_of_match', models.CharField(max_length=20)),
                ('venue', models.CharField(max_length=20)),
                ('umpire1', models.CharField(max_length=20)),
                ('umpire2', models.CharField(max_length=20)),
                ('umpire3', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='Delivery',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('inning', models.IntegerField()),
                ('batting_team', models.CharField(max_length=60)),
                ('bowling_team', models.CharField(max_length=60)),
                ('over', models.IntegerField()),
                ('ball', models.IntegerField()),
                ('batsman', models.CharField(max_length=30)),
                ('non_striker', models.CharField(max_length=30)),
                ('bowler', models.CharField(max_length=30)),
                ('is_super_over', models.BooleanField()),
                ('wide_runs', models.IntegerField()),
                ('bye_runs', models.IntegerField()),
                ('legbye_runs', models.IntegerField()),
                ('noball_runs', models.IntegerField()),
                ('penalty_runs', models.IntegerField()),
                ('batsman_runs', models.IntegerField()),
                ('extra_runs', models.IntegerField()),
                ('total_runs', models.IntegerField()),
                ('player_dismissed', models.CharField(max_length=30)),
                ('dismissal_kind', models.CharField(max_length=30)),
                ('fielder', models.CharField(max_length=30)),
                ('match', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='iplcloneapp.Match')),
            ],
        ),
    ]
