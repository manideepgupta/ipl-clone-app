from django.contrib.auth.models import User as authUser
from django.views import View
from iplcloneapp.models import *
from django.urls import resolve
from django.shortcuts import  render,get_object_or_404,redirect
from django.db.models import Count
from django.core.paginator import Paginator
from django.contrib.auth.mixins import LoginRequiredMixin,PermissionRequiredMixin
import logging
from django.contrib.auth.mixins import LoginRequiredMixin,PermissionRequiredMixin


class SeasonView(View):
    def get(self,request,*args,**kwargs):
        season = Match.objects.values('season').all().distinct()

        match=Match.objects.filter(season=kwargs.get('season'))
        paginator = Paginator(match, 8)
        page = request.GET.get('page')
        match = paginator.get_page(page)
        image=0
        if request.user.is_authenticated:
            image=ipluser.objects.get(user__username=request.user.username)
        return render(
            request,
            template_name="get_seasons.html",
            context={
            'matches':match,
            'seasons':season,
            'season':kwargs.get('season'),
            'image':image,
             }
        )
class DetailView(LoginRequiredMixin,View):
    login_url = '/login/'
    def get(self,request,*args,**kwargs):
        season = Match.objects.values('season').all().distinct()
        image = 0
        if request.user.is_authenticated:
            image = ipluser.objects.get(user__username=request.user.username)
        if (kwargs.get('matchid')):
            matchdetail = Match.objects.get(id=kwargs.get('matchid'))
            delivery = Delivery.objects.filter(match_id=kwargs.get('matchid'))
            top1 = delivery.values('batsman', 'batting_team').filter(inning=1).annotate(
                runs=Count('batsman_runs')).order_by('-runs')[:3]
            top2 = delivery.values('batsman', 'batting_team').filter(inning=2).annotate(
                runs=Count('batsman_runs')).order_by('-runs')[:3]
            d = Delivery.objects.filter(match_id=kwargs.get('matchid')).exclude(dismissal_kind='').values('bowler',
                                                                                                          'bowling_team')
            wicket1 = d.filter(inning=1).annotate(wickets=Count('bowler')).order_by('-wickets')[:3]
            wicket2 = d.filter(inning=2).annotate(wickets=Count('bowler')).order_by('-wickets')[:3]
            if (kwargs.get('inning')):
                commentary = Delivery.objects.values('over', 'ball', 'batsman', 'non_striker', 'bowler', 'total_runs',
                                                     'extra_runs', 'dismissal_kind', 'fielder').filter(
                    match_id=kwargs.get('matchid'), inning=kwargs.get('inning'))
            else:
                commentary = Delivery.objects.values('over', 'ball', 'batsman', 'non_striker', 'bowler', 'total_runs',
                                                     'extra_runs', 'dismissal_kind', 'fielder').filter(
                    match_id=kwargs.get('matchid'), inning=1)
            if kwargs.get('inning'):
                        innings=kwargs.get('inning')
            else:
                innings=1


            return render(
                request,
                template_name="get_match_details.html",
                context={
                    'mat': matchdetail,
                    'seasons': season,
                    'season': kwargs.get('season'),
                    'match': kwargs.get('matchid'),
                    'top1': top1,
                    'top2': top2,
                    'wicket1': wicket1,
                    'wicket2': wicket2,
                    'commentary': commentary,
                    'inning': innings,
                    'image':image,

                }
            )
class PointTableView(View):
    def get(self,request,*args,**kwargs):
        season = Match.objects.values('season').all().distinct()
        M=Match.objects.values('season','winner','result').filter(season=kwargs.get('season')).exclude(winner='None')
        points=M.values('winner').annotate(points=2*Count('winner')).order_by('-points')[:8]
        return render(
            request,
            template_name='pointstable.html',
            context={
                'points':points,
                'seasons':season,
                'season': kwargs.get('season'),
            }
        )
